FROM openjdk:11
ADD target/itschool-0.0.1.jar itschool-0.0.1.jar
EXPOSE 8080
CMD ["java", "-jar", "itschool-0.0.1.jar"]