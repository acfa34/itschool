package id.co.bankmandiri.microservice.itschool.repository;

import id.co.bankmandiri.microservice.itschool.model.LoginResponseDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class LoginRepository {
    @Autowired
    DataSource dataSource;
    public Object retrieveDetailUser(String user) throws EmptyResultDataAccessException, Exception {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {
            Object result = jdbcTemplate.queryForObject("select username, password, calledName, cif from hyduuser where username = ?",
                    new Object[]{user}, new LoginResponseDAO());
            return result;
        }catch (Exception e){
            return null;
        }
    }
}
