package id.co.bankmandiri.microservice.itschool.model;

import lombok.Data;

@Data
public class SavingResponse {
    String nama;
    String savingNo;
}
