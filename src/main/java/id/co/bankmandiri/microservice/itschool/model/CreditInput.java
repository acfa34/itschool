package id.co.bankmandiri.microservice.itschool.model;

import lombok.Data;

@Data
public class CreditInput {
    String cif;
    String nama;
    String limitPinjaman;
    String tujuan;
    String kota;
    String kecamatan;
}
