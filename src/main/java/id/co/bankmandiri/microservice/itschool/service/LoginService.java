package id.co.bankmandiri.microservice.itschool.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.bankmandiri.microservice.itschool.model.Login;
import id.co.bankmandiri.microservice.itschool.model.LoginResponse;
import id.co.bankmandiri.microservice.itschool.model.LoginResponseUser;
import id.co.bankmandiri.microservice.itschool.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class LoginService {
    @Autowired
    LoginRepository loginRepository;

    ObjectMapper objectMapper = new ObjectMapper();


    public Map<Object,Object> checkpassword(Login login) throws Exception {
        Map<Object, Object> result = new HashMap<>();
        LoginResponse responseDatabase=objectMapper.convertValue(loginRepository.retrieveDetailUser(login.getUsername()),LoginResponse.class);
        if(login.getPassword().equals(responseDatabase.getPassword())){
            result.put("text","pass");
            result.put("data",responseDatabase);
            return result;
        }
        else{
            result.put("text","failed");
            return result;
        }
    }
    public LoginResponseUser retrieveToUser(Login login) throws Exception {
        try {
            Map<Object, Object> result = new HashMap<>();
            LoginResponseUser loginResponseUser = new LoginResponseUser();
            result = checkpassword(login);
            if (result.get("text").equals("pass")) {
                LoginResponse loginResponse = objectMapper.convertValue(result.get("data"), LoginResponse.class);
                loginResponseUser.setMessage("Berhasil");
                loginResponseUser.setCif(loginResponse.getCif());
                loginResponseUser.setNama(loginResponse.getNama());
                return loginResponseUser;
            } else {
                loginResponseUser.setNama("null");
                loginResponseUser.setCif("null");
                loginResponseUser.setMessage("Password dan Username Anda Tidak Cocok");
                return loginResponseUser;
            }
        }catch ( Exception e){
            return null;
        }
    }
}
