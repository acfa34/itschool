package id.co.bankmandiri.microservice.itschool.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RekomendasiResponseDAO implements RowMapper {

    @Override
    public Object mapRow(ResultSet rs, int i) throws SQLException {
        RekomendasiResponse rekomendasiResponse = new RekomendasiResponse();
        rekomendasiResponse.setNama(rs.getString("nama"));
        rekomendasiResponse.setOverview(rs.getString("overview"));
        return rekomendasiResponse;
    }
}