package id.co.bankmandiri.microservice.itschool.model;

public class CifRequest {
    String cif;

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }
}
