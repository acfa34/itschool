package id.co.bankmandiri.microservice.itschool.model;

import lombok.Data;

import java.util.List;

@Data
public class ListSavingResponse {
    List<SavingResponse> savingResponses;

}
