package id.co.bankmandiri.microservice.itschool.model;

import lombok.Data;

@Data
public class StatusCredit {
    private String percentage;
    private String status;
    private String limit;
    private String tujuan;
}
