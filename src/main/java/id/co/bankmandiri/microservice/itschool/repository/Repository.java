package id.co.bankmandiri.microservice.itschool.repository;

import id.co.bankmandiri.microservice.itschool.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

@Component
public class Repository {
    @Autowired
    DataSource dataSource;
    public List<SavingResponse> retrieveDetailSaving(String cif) throws EmptyResultDataAccessException, Exception {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {
            List<SavingResponse> result = jdbcTemplate.query("select nama,savingNo from hydusaving where cif = ?",
                    new Object[]{cif}, new SavingResponseDAO());
            return result;
        }catch (Exception e){
            return null;
        }
    }


    public Object retrieveRekomendasi(String nama) throws EmptyResultDataAccessException, Exception {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {
            Object result = jdbcTemplate.queryForObject("select nama, overview from hyduproduct where nama = ?",
                    new Object[]{nama}, new RekomendasiResponseDAO());
            return result;
        }catch (Exception e){
            return null;
        }
    }

    public List<StatusCreditResponse> retrieveStatusCredit(String cif) throws EmptyResultDataAccessException, Exception {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {
            List<StatusCreditResponse> result = jdbcTemplate.query("select limitPinjaman,statusPinjaman,tujuan from hyducredit where cif = ?",
                    new Object[]{cif}, new StatusCreditResponseDAO());
            return result;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Object retrieveProfile(String cif) throws EmptyResultDataAccessException, Exception {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        try {
            Object result = jdbcTemplate.queryForObject("select nama,nik,cif from hyduuser where cif = ?",
                    new Object[]{cif}, new ProfileResponseDAO());
            return result;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    public boolean inputCredit(CreditInput creditInput) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        try {
        jdbcTemplate.update("insert into hyducredit(cif,nama,limitPinjaman,tujuan,kota,kecamatan) values (?,?,?,?,?,?)",
                new Object[]{creditInput.getCif(),creditInput.getNama(),creditInput.getLimitPinjaman(),
                        creditInput.getTujuan(),creditInput.getKota(),creditInput.getKecamatan()});
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }
}
