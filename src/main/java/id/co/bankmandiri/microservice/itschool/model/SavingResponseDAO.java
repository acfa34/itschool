package id.co.bankmandiri.microservice.itschool.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SavingResponseDAO implements RowMapper {

    @Override
    public Object mapRow(ResultSet rs, int i) throws SQLException {
        SavingResponse savingResponse= new SavingResponse();
        savingResponse.setNama(rs.getString("nama"));
        savingResponse.setSavingNo(rs.getString("savingNo"));
        return savingResponse;
    }
}

