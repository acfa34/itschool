package id.co.bankmandiri.microservice.itschool.model;

import lombok.Data;

@Data
public class ProfileResponse {
    String nama;
    String nik;
    String cif;
}
