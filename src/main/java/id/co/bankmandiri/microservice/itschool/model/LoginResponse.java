package id.co.bankmandiri.microservice.itschool.model;

import lombok.Data;

@Data
public class LoginResponse {
    String username;
    String password;
    String nama;
    String cif;

}
