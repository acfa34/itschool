package id.co.bankmandiri.microservice.itschool.model;

import lombok.Data;

@Data
public class RekomendasiResponse {
    String nama;
    String overview;
}
