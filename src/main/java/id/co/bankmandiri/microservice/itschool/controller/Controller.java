package id.co.bankmandiri.microservice.itschool.controller;

import id.co.bankmandiri.microservice.itschool.model.*;
import id.co.bankmandiri.microservice.itschool.repository.Repository;
import id.co.bankmandiri.microservice.itschool.service.LoginService;
import id.co.bankmandiri.microservice.itschool.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Controller {
    @Autowired
    LoginService loginService;

    @Autowired
    Service service;

    @Autowired
    Repository repository;

    @PostMapping("/login")
    public ResponseEntity<?> retrieveData(@RequestBody Login login) throws Exception {
        try {
            return new ResponseEntity<>(loginService.retrieveToUser(login), HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/retrieveSaving")
    public ResponseEntity<?> retrieveData(@RequestBody CifRequest cifRequest) throws Exception {
        try {
            return new ResponseEntity<>(service.retrieveSavingAccount(cifRequest.getCif()), HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("/retrieveRekomendasi")
    public ResponseEntity<?> retrieveData(@RequestBody RekomendasiRequest rekomendasiRequest) throws Exception {
        try {
            return new ResponseEntity<>(service.retrieveRekomendasi(rekomendasiRequest), HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/retrieveStatusCredit")
    public ResponseEntity<?> retrieveStatusCredit(@RequestBody CifRequest cifRequest) throws Exception {
        try {
            return new ResponseEntity<>(service.retrieveStatusCredit(cifRequest.getCif()), HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/retrieveProfile")
    public ResponseEntity<?> retrieveProfile(@RequestBody CifRequest cifRequest) throws Exception {
        try {
            return new ResponseEntity<>(service.retrieveProfile(cifRequest.getCif()), HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/inputCredit")
    public ResponseEntity<Boolean> retrieveProfile(@RequestBody CreditInput creditInput) throws Exception {
        try {
            return new ResponseEntity<>(service.inputCredit(creditInput), HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
