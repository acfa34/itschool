package id.co.bankmandiri.microservice.itschool.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.bankmandiri.microservice.itschool.model.*;
import id.co.bankmandiri.microservice.itschool.repository.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class Service {
    @Autowired
    Repository repository;

    ObjectMapper objectMapper = new ObjectMapper();
    Map<Object, Object> result = new HashMap<>();
    RekomendasiResponse rekomendasiResponse = new RekomendasiResponse();

    public ListSavingResponse retrieveSavingAccount(String cif) throws Exception {
        ListSavingResponse listSavingResponse = new ListSavingResponse();
        listSavingResponse.setSavingResponses(repository.retrieveDetailSaving(cif));
        return listSavingResponse;
    }
    public RekomendasiResponse retrieveRekomendasi(RekomendasiRequest rekomendasiRequest) throws Exception {
        int limit = Integer.parseInt(rekomendasiRequest.getLimit());
        if (rekomendasiRequest.getTujuan().equals("Usaha") && limit<=25000000){
            rekomendasiResponse=objectMapper.convertValue(repository.retrieveRekomendasi("KUR"),RekomendasiResponse.class);
            return rekomendasiResponse;
        }else if(rekomendasiRequest.getTujuan().equals("Usaha") && limit>25000000){
            rekomendasiResponse=objectMapper.convertValue(repository.retrieveRekomendasi("KUM"),RekomendasiResponse.class);
            return rekomendasiResponse;
        }else if(rekomendasiRequest.getTujuan().equals("Konsumtif")){
            rekomendasiResponse=objectMapper.convertValue(repository.retrieveRekomendasi("KSM"),RekomendasiResponse.class);
            return rekomendasiResponse;
        }else if(rekomendasiRequest.getTujuan().equals("Pendidikan")){
            rekomendasiResponse=objectMapper.convertValue(repository.retrieveRekomendasi("Student Loan"),RekomendasiResponse.class);
            return rekomendasiResponse;
        }
            rekomendasiResponse.setNama(null);
            rekomendasiResponse.setOverview(null);
            return rekomendasiResponse;
    }
    public ListStatusCredit retrieveStatusCredit(String cif) throws Exception {
        List<StatusCredit> listStatusCredit = new ArrayList<>();
        ListStatusCreditResponse response = new ListStatusCreditResponse();
        ListStatusCredit listStatusCredit1 = new ListStatusCredit();
        response.setStatusCreditResponse(repository.retrieveStatusCredit(cif));
        for(int i=0;i<response.getStatusCreditResponse().size();i++){
            StatusCredit statusCredit =new StatusCredit();
            statusCredit.setLimit(response.getStatusCreditResponse().get(i).getLimit());
            statusCredit.setTujuan(response.getStatusCreditResponse().get(i).getTujuan());
            if(response.getStatusCreditResponse().get(i).getStatus().equals("1")){
                statusCredit.setPercentage("25");
                statusCredit.setStatus("Data Telah Tersubmit");
            }else if(response.getStatusCreditResponse().get(i).getStatus().equals("2")){
                statusCredit.setPercentage("50");
                statusCredit.setStatus("Data Pinjaman Sedang Diverifikasi");
            }else if(response.getStatusCreditResponse().get(i).getStatus().equals("3")){
                statusCredit.setPercentage("75");
                statusCredit.setStatus("Data Pinjaman Sedang Divalidasi");
            }else if(response.getStatusCreditResponse().get(i).getStatus().equals("4")){
                statusCredit.setPercentage("100");
                statusCredit.setStatus("Pinjaman anda Diterima");
            }else{
                statusCredit.setPercentage("0");
                statusCredit.setStatus("Pinjaman anda Ditolak");
            }
            listStatusCredit.add(i,statusCredit);
        }
        listStatusCredit1.setStatusCredits(listStatusCredit);
        return listStatusCredit1;

    }
    public ProfileResponse retrieveProfile(String cif) throws Exception {
        ProfileResponse profileResponse=objectMapper.convertValue(repository.retrieveProfile(cif),ProfileResponse.class);
        return profileResponse;
    }
    public boolean inputCredit(CreditInput creditInput) throws Exception {
        return repository.inputCredit(creditInput);
    }
}

