package id.co.bankmandiri.microservice.itschool.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProfileResponseDAO implements RowMapper {
    @Override
    public Object mapRow(ResultSet rs, int i) throws SQLException {
        ProfileResponse profileResponse = new ProfileResponse();
        profileResponse.setNama(rs.getString("nama"));
        profileResponse.setCif(rs.getString("cif"));
        profileResponse.setNik(rs.getString("nik"));
        return profileResponse;
    }
}
