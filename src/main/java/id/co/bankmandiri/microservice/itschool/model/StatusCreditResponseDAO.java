package id.co.bankmandiri.microservice.itschool.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StatusCreditResponseDAO implements RowMapper {

    @Override
    public Object mapRow(ResultSet rs, int i) throws SQLException {
        StatusCreditResponse statusCreditResponse = new StatusCreditResponse();
        statusCreditResponse.setLimit(rs.getString("limitPinjaman"));
        statusCreditResponse.setStatus(rs.getString("statusPinjaman"));
        statusCreditResponse.setTujuan(rs.getString("tujuan"));
        return statusCreditResponse;
    }
}
