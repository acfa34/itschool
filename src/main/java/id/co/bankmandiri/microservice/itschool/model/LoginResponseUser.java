package id.co.bankmandiri.microservice.itschool.model;

import lombok.Data;

@Data
public class LoginResponseUser {
    String message;
    String nama;
    String cif;

}
