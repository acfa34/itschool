package id.co.bankmandiri.microservice.itschool.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginResponseDAO implements RowMapper {

        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {
           LoginResponse loginResponse = new LoginResponse();
            loginResponse.setUsername(rs.getString("username"));
            loginResponse.setPassword(rs.getString("password"));
            loginResponse.setNama(rs.getString("calledName"));
            loginResponse.setCif(rs.getString("cif"));
            return loginResponse;
        }
}
