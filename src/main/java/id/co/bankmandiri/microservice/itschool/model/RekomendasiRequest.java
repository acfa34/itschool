package id.co.bankmandiri.microservice.itschool.model;

import lombok.Data;

@Data
public class RekomendasiRequest {
    String tujuan;
    String tenor;
    String limit;

}
