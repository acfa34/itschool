package id.co.bankmandiri.microservice.itschool.model;

import lombok.Data;

@Data
public class Login {
    String username;
    String password;

}
